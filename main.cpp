#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <cmath>
using namespace std;

//Variablen
const string prafix = "y", prafix2 = "z";
string eingabe, name, zahlstring,  datei, datei2;
double  temp1 = 0, temp2 = 0, temp3 = 0;
double zeit=0, ampli= 0, ampliges = 0, erstezeit = 0, austemp1 = 0;
unsigned int zahler = 0;
int menge = 0, posit = -1, zwzahler = 0;
ifstream eing, dat, eing2;
ofstream ausg, index, index2, ausg2;
bool nulldurch = false;
double amplimax = 0, amplimin = 0, mittel = 0, stand = 0, quad = 0, nulllage = 0, nullext = 0;
int exzahler = 0, nullzahler = 0; 
double halbperiode = 0, rechzahl = 0;
string mot, namemot, nameext;

//Funktionen
int zahl(string nam);
bool istnull(double hmgucken);
void extremamitteln(string nameext);
void periode(string namepe);
void motnulldurch(bool zmot, double xmot, double ymot, string namemot);
string plot();

//UnterProgramme von Felix
void findZero2(double *x, double *y, string ausgabename, int anz);
void findExtrema(double *x, double *y, string ausgabename, int anz);
void findZero(double *x, double *y, string ausgabename, int anz);

int main(){

eing.open("index.txt");
index.open("indexneu.txt");
while(!eing.eof()){
	zwzahler = 0;
	erstezeit = 0;
	menge = 0;
	nulldurch = false;
	eing >> name;
	cout << name << " ";
	datei = prafix;
	datei += name;

	if(name == "1"){posit = 1;index << posit << endl; continue;}

	if(name == "0"){posit = 0;index << posit << endl; continue;}

	if(name == "2"){posit = 2;index << posit << endl; continue;}

	if(name == "4"){posit = 4;index << posit << endl; continue;}

	if(name == "6"){posit = 6;index << posit << endl;continue;}

	if(name == "8"){posit = 8;index << posit << endl; continue;}


	if(posit == 1 || posit == 2 || posit == 4 || posit == 6 || posit == 8){
		menge = zahl(name);
		dat.open(name.c_str());
		for(int i =0;i <13;i++){dat >> eingabe;}
		ausg.open(datei.c_str());
		ausg << menge << endl;
		cout << menge << " ";
		while(!dat.eof()){
			dat >> temp1 >> temp2 >> temp3;
			if(zwzahler == 0){
				erstezeit=temp1;
			}
			austemp1 = temp1-erstezeit;
			ausg  << austemp1 << " " <<  temp2 << " " << temp3 <<  endl;
		zwzahler++;
		}
	}
	
	if(posit == 0){
		datei2 = prafix2;
		datei2 += name;
		//menge = zahl(name);
		dat.open(name.c_str());
		for(int i =0;i <13;i++){dat >> eingabe;}
		ausg.open(datei2.c_str());
		//ausg << menge << endl;
		//cout << menge << " " << endl;
		while(!dat.eof()){
			dat >> temp1 >> temp2 >> temp3;	
			if(!nulldurch){
				//cout << "Te" << endl;
				if(!istnull(temp2)){
					nulldurch = true;
					ausg  << temp1 << " " <<  temp2 << " " << temp3 <<  endl;
					continue;
				}
				continue;
			}	
			ausg  << temp1 << " " <<  temp2 << " " << temp3 <<  endl;
		}
		dat.close();
		ausg.close();
		menge = zahl(datei2);
		dat.open(datei2.c_str());
		ausg.open(datei.c_str());
		ausg << menge << endl;
		while(!dat.eof()){
			dat >> temp1 >> temp2 >> temp3;
			if(zwzahler == 0){
				erstezeit = temp1;
			}
			austemp1 = temp1 - erstezeit;
			ausg << austemp1 << " " << temp2 << " " << temp3 << endl;
			
			zwzahler++;
		}
	}
	
	dat.close();
	ausg.close();
	index << datei << endl;
	cout << datei << endl;
	zahler++;
}
eing.close();
index.close();


//Bearbeiten der Daten erneut zur Normierung der Daten
eing.open("indexneu.txt");
index.open("extremindex.txt");
index2.open("motindex.txt");
//Programm von Felix
while(!eing.eof()){
	eing >> name;

	if(name == "1"){posit = 1;index << posit << endl; continue;}

	if(name == "0"){posit = 0;index << posit << endl; continue;}

	if(name == "2"){posit = 2;index << posit << endl;index2 << posit << endl; continue;}

	if(name == "4"){posit = 4;index << posit << endl;index2 << posit << endl; continue;}

	if(name == "6"){posit = 6;index << posit << endl;index2 << posit << endl; continue;}

	if(name == "8"){posit = 8;index << posit << endl;index2 << posit << endl; continue;}

	string dat=name;//"../d6_f400";
	string ext=dat+"_extrema.txt";
	string zero=dat+"_Nullstellen.txt";
	string zero2=dat+"_Nullstellen2.txt";
	mot =dat+"_motnull.txt";
	//dat+=".txt";

	ifstream eingabef(dat.c_str());
	int lines;
	eingabef >> lines;
	double x[lines];
	double y[lines];
	bool   z[lines];
	if(!(posit == 1 || posit == 0)){ausg.open(mot.c_str());}
	for (int i=0; i<lines; i++) {
		eingabef >> x[i];
		eingabef >> y[i];
		eingabef >> z[i];
		if(posit != 1 || posit != 0){motnulldurch(z[i],x[i],y[i],mot);}
	}
	if(posit != 1 || posit != 0){ausg.close();}

	double avg=0;
	for (int i=0; i<lines; i++) {
		avg+= y[i];
	}
	avg/=(double) lines;
	//cout << avg << endl;



	findExtrema(x, y, ext, lines);
	//findZero(x, y, zero, lines);
	//findZero2(x, y, zero2, lines);
	index << ext << endl;
	if(!(posit == 1|| posit == 0)){index2 << ext << " "  << mot << endl;}
	cout << dat << " " << lines << endl;
}
index.close();
eing.close();
index2.close();

//Extrema mitteln
ausg.close();
eing.open("extremindex.txt");
zahler = 0;
while(!eing.eof()){
	eing >> name;
	if(name == "1"){zahler = 0;posit = 1;continue;}

	if(name == "0"){zahler = 0;posit = 0;continue;}

	if(name == "2"){zahler = 0;posit = 2;ausg.close();ausg.open("yd2_ext.txt"); continue;}

	if(name == "4"){zahler = 0;posit = 4;ausg.close();ausg.open("yd4_ext.txt"); continue;}

	if(name == "6"){zahler = 0;posit = 6;ausg.close();ausg.open("yd6_ext.txt");continue;}

	if(name == "8"){zahler = 0;posit = 8;ausg.close();ausg.open("yd8_ext.txt");continue;} 

	if(posit == 1){continue;}
	if(posit == 0){continue;}
	
	if(zahler == 0){
		if(posit == 2){
			ausg << "200 ";
			extremamitteln(name);
		}
		if(posit ==8){}
		if(posit == 4||posit == 6){
			ausg << "100 ";
			extremamitteln(name);

		}
	}

	
	if(zahler == 1){
		if(posit == 2){
			ausg << "300 ";
			extremamitteln(name);

		}
		if(posit == 4 || posit == 6){
			ausg << "200 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "100 ";
			extremamitteln(name);

		}
	}
	if(zahler == 2){
		if(posit==2){
			ausg << "330 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "200 ";
			extremamitteln(name);

		}
		if(posit == 4 || posit == 6){
			ausg << "250 " ;
			extremamitteln(name);

		}
	}

	if(zahler == 3){
		if(posit == 2){
			ausg << "350 ";
			extremamitteln(name);

		}
		if(posit == 4){
			ausg << "300 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "280 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "290 ";
			extremamitteln(name);

		}
	}
	if(zahler == 4){
		if(posit == 2){
			ausg << "400 ";
			extremamitteln(name);

		}
		if(posit == 4){
			ausg << "310 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "300 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "300 ";
			extremamitteln(name);

		}
	}
	if(zahler == 5){
		if(posit == 2){
			//continue;
		
		}
		if(posit == 4){
			ausg << "315 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "310 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "305 ";
			extremamitteln(name);

		}
	}
	if(zahler == 6){
		if(posit == 2){
			//continue;
		
		}
		if(posit == 4){
			ausg << "318 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "313 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "310 ";
			extremamitteln(name);

		}
	}
	if(zahler == 7){
		if(posit == 4){
			ausg << "320 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "315 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "320 ";
			extremamitteln(name);

		}
	}
	if(zahler == 8){
		if(posit == 4){
			ausg << "325 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "320 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "400 ";
			extremamitteln(name);

		}
	}
	if(zahler == 9){
		if(posit == 4){
			ausg << "330 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "330 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "500 ";
			extremamitteln(name);

		}
	}
	if(zahler == 10){
		if(posit == 4){
			ausg << "350 ";
			extremamitteln(name);

		}
		if(posit == 6){
			ausg << "340 ";
			extremamitteln(name);

		}
		if(posit == 8){
			ausg << "600 ";
			extremamitteln(name);

		}
	}
	if(zahler == 11){
		if(posit == 4 || posit == 6){
			ausg << "400 ";
			extremamitteln(name);

		}
	}
	if(zahler == 12){
		if(posit == 4 || posit == 6 ){
			ausg << "500 ";
			extremamitteln(name);

		}
	}
	if(zahler == 13){
		if(posit == 4 || posit == 6){
			ausg << "600 ";
			extremamitteln(name);

		}
	}
	zahler++;
}
eing.close();
ausg.close();

//neues Programm von Felix

string datfe[]={"yd0_f0.txt_extrema.txt","yd4_f0.txt_extrema.txt","yd6_f0_neu.txt_extrema.txt","yd8_f0_neu.txt_extrema.txt"};
int daempfung[]={0,4,6,8};
double  beta[]={0.0171504,0.15415,0.308362,0.465396}, betaSi[]={5.53*pow(10,-5),0.002541,0.009086,0.01356} ;
ofstream out("periode.txt");
double T[4],T_Si[4],logDek[4], logDekSi[4], omega_e[4], omega_e_Si[4], omega_0[4], omega_0_Si[4], omega0, omega0_Si;
//double temp1=0, temp2=0;

	for(int i=0;i<4;i++) {
		cout << i << endl;
		string a=datfe[i];
		ifstream extremadaten(a.c_str());
		double arr[3][200], halbp=0, halbpSi=0;
		int k=0;
		for(int j=0;j<200;j++) {arr[0][j]=0; arr[1][j]=0; arr[2][j]=0;}
		while(!extremadaten.eof()) {
			extremadaten >> arr[0][k] >> arr[1][k] >> arr[2][k] ;
			k++;
		}
		k--;
		halbp=(arr[0][k-1]-arr[0][0])/(double)(k-1);
		for(int l=1; l<k; l++) { halbpSi+=pow(halbp-(arr[0][l]-arr[0][l-1]),2); }
		halbpSi=sqrt(halbpSi/(k*(k-1)));

		T[i]=2*halbp/1000.;
		T_Si[i]=2*halbpSi/1000.;

		omega_e[i]=2*M_PI/T[i];
		omega_e_Si[i]=2*M_PI*T_Si[i]/pow(T[i],2);

		logDek[i]=beta[i]*T[i];
		logDekSi[i]=sqrt(pow(T[i]*betaSi[i],2)+pow(T_Si[i]*beta[i],2));
		cout << logDek[i] << " " << logDekSi[i] << endl;
		omega_0[i]=sqrt(pow(omega_e[i],2)+pow(beta[i],2));
		omega_0_Si[i]=sqrt(pow(omega_e[i]*omega_e_Si[i],2)+pow(beta[i]*betaSi[i],2))/omega_0[i];

		out << daempfung[i] << "\t" << beta[i] << "\t" << betaSi[i] << "\t"  << T[i] << "\t" << T_Si[i] << "\t" << omega_e[i] << "\t" << omega_e_Si[i] << "\t";
		out << logDek[i] << "\t" << logDekSi[i] << "\t" << omega_0[i] << "\t" << omega_0_Si[i] << endl;
		
		cout << omega_e[i] << "\t" << omega_e_Si[i] << "\t" << omega_0[i] << "\t" << omega_0_Si[i] << endl;
		extremadaten.close();
		cout << endl;
	}
temp1=0;
temp2=0;
for(int i=0;i<4;i++) {
	temp1+=(omega_0[i]/pow(omega_0_Si[i],2));
	temp2+=pow(omega_0_Si[i],-2);
}
omega0=temp1/temp2;
omega0_Si=pow(temp2,-0.5);
out << omega0 << "\t" << omega0_Si << endl;
for(int i =0; i<4;i++){
	out 	<< (sqrt(pow(omega0,2)-2*pow(beta[i],2))) << "\t" 
		<< (sqrt(pow(omega0*omega0_Si,2)+pow(2*beta[i]*betaSi[i],2))/(sqrt(pow(omega0,2)+2*pow(beta[i],2))))
		<<  endl;

} 
out.close();

string gnuplot = "gnuplot ";
gnuplot +=plot();

system(gnuplot.c_str());
//system("make Prohaupt.pdf");

return 0;
}

string plot(){
string plotname = "plot.gp";
ofstream plotdat(plotname.c_str());
plotdat << "reset" << endl;
plotdat << "set term postscript enhanced color 'Roman' 21" << endl;
plotdat << "set grid" << endl;
plotdat << "set output 'freqamplimax.eps'" << endl;
plotdat << "set xlabel 'Frequenz [mHz]'" << endl;
plotdat << "set ylabel 'Amplitude [grad]" << endl;
plotdat << "p 'yd2_ext.txt' u 1:2 t 'Daempfung 2mm'  w lp, 'yd4_ext.txt' u 1:2 t 'Daempfung 4mm'  w lp, 'yd6_ext.txt' u 1:2 t 'Daempfung 6mm'  w lp, 'yd8_ext.txt' u 1:2 t 'Daempfung 8mm'  w lp" << endl;
plotdat << "set output" << endl;
plotdat << "set output 'Phase.eps'" << endl;
plotdat << "set xlabel 'omega/omega0 [mHz]'" << endl;
plotdat << "set ylabel 'Phase [Phi / PI]'" << endl;
plotdat << "set key bottom" << endl;
plotdat << "p 'yd2_ext.txt' u 1:7 t 'Daempfung 2mm'  w lp, 'yd4_ext.txt' u 1:7 t 'Daempfung 4mm'  w lp, 'yd6_ext.txt' u 1:7 t 'Daempfung 6mm'  w lp, 'yd8_ext.txt' u 1:7 t 'Daempfung 8mm'  w lp" << endl;
plotdat << "set key top" << endl;
plotdat << "set output 'pd0_f0.eps'" << endl;
plotdat << "set ylabel 'Winkel [rad]'" << endl;
plotdat << "set xlabel 'Zeit [s]" << endl;
plotdat << "p 'yd0_f0.txt' u ($1/1000):($2+3.375) t 'Daempfung 0mm'" << endl;
plotdat << "set output 'pd4_f0.eps'" << endl;
plotdat << "p 'yd4_f0.txt' u ($1/1000):($2+3.375) t 'Daempfung 4mm'" << endl;
plotdat << "set output 'pd6_f0.eps'" << endl;
plotdat << "p 'yd6_f0_neu.txt' u ($1/1000):($2+3.375) t 'Daempfung 6mm'" << endl;
plotdat << "set output 'pd8_f0.eps'" << endl;
plotdat << "p 'yd8_f0_neu.txt' u ($1/1000):($2+3.375) t 'Daempfung 8mm'" << endl;
//Logarithmisches Dekrement
/*
plotdat << "f(x)=m*x+b" << endl;
plotdat << "set fit logfile 'yd0_f0_log.txt'" << endl;
plotdat << "fit f(x) 'yd0_f0.txt_extrema.txt' u 1:(log($2)):3 via m,b" << endl;
plotdat << "set fit logfile 'yd4_f0_log.txt'" << endl;
plotdat << "fit f(x) 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3 via m,b" << endl;
plotdat << "set fit logfile 'yd6_f0_neu_log.txt'" << endl;
plotdat << "fit f(x) 'yd6_f0_neu.txt_extrema.txt' u 1:(log($2)):3 via m,b" << endl;
plotdat << "set fit logfile 'yd8_f0_neu_log.txt'" << endl;
plotdat << "fit f(x) 'yd8_f0_neu.txt_extrema.txt' u 1:(log($2)):3 via m,b" << endl;
*/

//plotdat << "!epstopdf --autorotate=All Donut.eps" << endl;
//plotdat << "pause -1" << endl;
plotdat.close();
return plotname;
}

void motnulldurch(bool zmot, double xmot, double ymot, string namemot){
	if(zmot){
		ausg << xmot << " " << ymot  << endl;
	} 

}


void periode(string namepe){
	zwzahler = 0;
	dat.close();
	dat.open(namepe.c_str());
	while(!dat.eof()){
		dat >> temp1 >> temp2 >> temp3;		
		zwzahler++;
	}
	zwzahler--;
	//cout << zwzahler << endl;
	dat.close();
	double zeitext[zwzahler];
	dat.open(namepe.c_str());
	for(int i=0; i<zwzahler;i++){
		dat >> zeitext[i] >> temp2 >> temp3;
	//	cout << zeitext[i] << endl;
	}
	stand = 0;
	quad = 0;
	halbperiode = (zeitext[zwzahler-1]-zeitext[0])/(zwzahler-1);
	for(int i = 0; i < (zwzahler-1);i++){
		quad += pow(((zeitext[i+1]-zeitext[i])-halbperiode),2); 
	}
	rechzahl = ((zwzahler-1) * (zwzahler -2));
	stand = sqrt((1/rechzahl)*quad);
	ausg << halbperiode << " " << stand;
	//cout << halbperiode << " " << stand << endl;
	dat.close();
	
	//Nulldurchgang
    double nullextrem[200], motzeit[200], tempzeit = 0, motamp[200], mittelmot=0, mitmotzeit = 0, verschiebung[200], verschSi=0;
	
    for(int i=0; i<200; i++){nullextrem[i]=0; motzeit[i]=0; motamp[i]=0; verschiebung[i]=0;}
	
	datei = namepe+"null.txt";
	//cout << datei << endl;
	ausg2.open(datei.c_str());	
	for(int i = 0; i <((zwzahler-1)/2); i++){
		nullextrem[i]=(zeitext[2*i+1]+zeitext[2*i])/2;
		ausg2 << nullextrem[i] << " " << "-3.375" << endl;
	}
	eing2.open("motindex.txt");	
	while(!(eing2.eof())){
        verschSi = 0;
        mittelmot = 0;
		mitmotzeit = 0;
		eing2 >> nameext;
		if(nameext == "2"){continue;}
		if(nameext == "4"){continue;}
		if(nameext == "6"){continue;}
		if(nameext == "8"){continue;}
		eing2 >> namemot;
		if(nameext == namepe){
			dat.open(namemot.c_str());
			cout << namemot << " " << (zwzahler-1)/2  << endl;
			int k = 0;

            while(!(dat.eof()) && k < ((zwzahler-1)/2)){
				dat >> motzeit[k] >> motamp[k];
				mittelmot +=motamp[k];
				
				tempzeit = (nullextrem[k]-motzeit[k]);
                while(tempzeit < 0){tempzeit+=halbperiode;}
                while(tempzeit > (halbperiode+stand)){tempzeit-= halbperiode;}
				mitmotzeit += tempzeit;
                verschiebung[k]=tempzeit/halbperiode;
                cout << verschiebung [k] << "  ";
				//cout << motamp[k] << " " ;	
				//ausg2 << nullextrem[k] << " " << " "  << endl;
				k++;			
			}
            cout << endl  << k << " " << mitmotzeit << " " << halbperiode << " ";
            mitmotzeit /= (double)(k);
			mitmotzeit /= halbperiode;
            for (int i=0; i<k; i++) {
                cout << endl << verschSi;
                verschSi+=pow(mitmotzeit-verschiebung[i],2);
            }
            verschSi=sqrt(verschSi/(k*(k-1)));

			//while(mitmotzeit < 0){mitmotzeit += 1;}
            cout << mitmotzeit << "  " << verschSi <<endl;
			//cout << endl << mittelmot << endl;; 	
            mittelmot /= ((double)(k));
			mittelmot /= amplimin;
            ausg <<  " " << asin(mittelmot) << " "  << mitmotzeit << " " << verschSi << endl;
			dat.close();
		}
	}
	eing2.close();
	ausg2.close();
}


void extremamitteln(string nameext){
//Mittelwert
	amplimax = 0;
	amplimin = 0;
	zwzahler = 0;
	exzahler = 0;
	dat.open(nameext.c_str());
	while(!dat.eof()){
		dat >> temp1 >> temp2 >> temp3;
		if(temp2 > 0){
			amplimax += temp2;
			zwzahler++;
		}
		if(temp2 < 0){
			amplimin += temp2;
			exzahler++;
		}
	}
	amplimax /=zwzahler;
	amplimin /=exzahler;
	mittel = (amplimax - amplimin)/2;
	ausg << mittel << " " << (amplimax + amplimin)/2. << " 0.75  ";
	//cout << (amplimax + amplimin)/2. << endl;
	dat.close();
	
	//Peroiode Mitteln
	periode(nameext);
	
//Standartabweichung
/*	
	amplimax = 0;
	stand = 0;
	quad = 0;
	zwzahler = 0;
	exzahler = 0;
	dat.open(name.c_str());
	while(!dat.eof()){
		dat >> temp1 >> temp2 >> temp3;
		if(temp2 > 0){
			amplimax += (temp2-mittel)*(temp2-mittel);
			zwzahler++;
		}
		if(temp2 < 0){
			amplimin += (temp2 + mittel)*(temp2+mittel);
			exzahler++;
		}
	}
	cout << amplimax << " ";
	amplimax +=amplimin;
	zwzahler += exzahler;
	quad = ((zwzahler -1));
	cout << quad << " ";
	stand = sqrt((1/quad)*amplimax);
	ausg << stand << endl;
	cout << stand << endl;
	nulllage += stand;
	nullzahler++;
	dat.close();
*/
	
}

int zahl(string nam){
	int anzahl = 0;
	dat.open(nam.c_str());
	for (int i = 0; i<13;i++){dat >> eingabe;}
	while(!dat.eof()){
		dat >> temp1 >> temp2 >> temp3;
		anzahl++;
	}
	dat.close();
	cout <<  anzahl <<  endl; 
	return anzahl;
}

bool istnull(double hmgucken){
if(hmgucken == 0){return true;}
else{return false;}
}

//Unterprogramme von Felix
void findExtrema(double *x, double *y, string ausgabename, int anz) {
	ofstream ausgabe(ausgabename.c_str());
	int minOrMax=1;
	int k=0;
	while (y[k]==y[k+1]) {
		k++;
	}
	if(y[k]>y[k+1]) {
		minOrMax=-1;
	}
	double a,b;
	for (int i=k; i<anz-1; i++) {
		a=minOrMax*y[i];
		b=minOrMax*y[i+1];
		if(a>b) {
			int j=i;
			minOrMax*=-1;
			while (y[j]==y[i]) {
				j--;
			}
		ausgabe << (x[i]+x[j])/2 << "  " << y[i] << " " << "0.375" << endl;
		}
	}
	ausgabe.close();
}

void findZero(double *x, double *y, string ausgabename, int anz) {
    ofstream ausgabe(ausgabename.c_str());
    for (int i=1; i<anz; i++) {
        if(y[i]*y[i-1]<=0) {
            ausgabe << x[i-1] << "  " << y[i-1] << endl;
            ausgabe << x[i] << "  " << y[i] << endl;
        }

    }
    ausgabe.close();
}

void findZero2(double *x, double *y, string ausgabename, int anz) {
    ofstream ausgabe(ausgabename.c_str());
    for (int i=0; i<anz; i++) {
        if(y[i]==0.) {
            ausgabe << x[i] << "  " << y[i] << endl;
        }

    }
    ausgabe.close();
}
