reset
set term epslatex color
set output 'freqamplimax.tex'
set xlabel '$\omega/\overline{\omega_0}$'
set ylabel '$\varphi_0(\omega)/\varphi_0(0)$'

b=0.96

#f(x)=(1/(sqrt(2*pi*a)))*c*exp(-((x-b)**2)/(2*a**2))+1

j(x)=(a/(sqrt((b**2-x**2)**2+4*c**2*x**2)))
k(x)=(d/(sqrt((e**2-x**2)**2+4*f**2*x**2)))
l(x)=(g/(sqrt((h**2-x**2)**2+4*i**2*x**2)))
z(x)=(y/(sqrt((w**2-x**2)**2+4*t**2*x**2)))

#set xrange [0.5:1.5]

#fit f(x) "yd8_ext.txt" u ((pi*$1)/(500*2.04659)):($2/10) via a,b,c

set fit logfile "freqamplimax4.log"
fit j(x) 'yd4_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) via a,b,c
set fit logfile "freqamplimax6.log"
fit k(x) 'yd6_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) via d,e,f
set fit logfile "freqamplimax8.log"
fit l(x) 'yd8_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) via g,h,i
set fit logfile "freqamplimax2.log"
fit z(x) 'yd2_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) via y,w,t


#p f(x) t " " , "yd8_ext.txt" u ((pi*$1)/(500*2.04659)):($2/10) t " "

p j(x) t "fit Bremse 4mm" lt -1, 'yd4_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10):($4/10) w e t "Bremse 4mm" pt 7, k(x) t "fit Bremse 6mm" lt -1, 'yd6_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10):($4/10) w e t "Bremse 6mm" pt 88, l(x) t "fit Bremse 8mm" lt -1, 'yd8_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10):($4/10) w e t "Bremse 8mm" lc 1 pt 82, 'yd2_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10):($4/10) w e t "Bremse 2mm" lc 1 pt 2#, z(x) t "fit Bremse 2mm" lt -1

set output
!epstopdf freqamplimax.eps
#pause -1
