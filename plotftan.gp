reset
set term epslatex color
set output 'Phase.tex'
set xlabel '$\omega/\overline{\omega_0}$'
set ylabel 'Phasenverschiebung $[\phi/\pi]$'

b=1
a=0.168
set key bottom right
f(x)=atan((2*a*x)/(b**2-x**2))/pi
g(x)=(x<b)?f(x):f(x)+1
j(x)=atan((2*c*x)/(d**2-x**2))/pi
h(x)=(x<d)?j(x):j(x)+1
k(x)=atan((2*e*x)/(m**2-x**2))/pi
i(x)=(x<m)?k(x):k(x)+1

set xrange [0:2]

fit g(x) 'yd4_ext.txt' u (pi*$1/500/2.04659):8  via a,b
fit h(x) 'yd6_ext.txt' u (pi*$1/500/2.04659):8  via c,d
fit i(x) 'yd8_ext.txt' u (pi*$1/500/2.04659):8  via e,m

p g(x) t "fit Bremse 4mm" lc "black", 'yd4_ext.txt' u (pi*$1/500/2.04659):8:9 w e t "Bremse 4mm", h(x) t "fit Bremse 6mm" lc "black", 'yd6_ext.txt' u (pi*$1/500/2.04659):8:9 w e t "Bremse 6mm", i(x) t "fit Bremse 8mm" lc "black", 'yd8_ext.txt' u (pi*$1/500/2.04659):8:9 w e t "Bremse 8mm" lc 1

set output
!epstopdf Phase.eps
