reset
set term epslatex color
set output 'freqamplimax.tex'
set xlabel '$\omega/\omega_0$'
set ylabel '$\varphi_0(\omega)/\varphi_0(0)$'
p 'yd2_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 2mm', 'yd4_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 4mm'  w lp, 'yd6_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 6mm' w lp, 'yd8_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 8mm' w lp
set output
set output 'Phase.tex'
set xlabel '$\omega/\omega_0$'
set ylabel 'Phasenverschiebung $[\phi/\pi]$'
set key bottom
p 'yd2_ext.txt' u ((pi*$1)/(500*2.04659)):7 t 'Bremse 2mm', 'yd4_ext.txt' u (pi*$1/500/2.04659):7 t 'Bremse 4mm'  w lp, 'yd6_ext.txt' u (pi*$1/500/2.04659):7 t 'Bremse 6mm'  w lp, 'yd8_ext.txt' u (pi*$1/500/2.04659):7 t 'Bremse 8mm'  w lp
set key top
set output 'pd0_f0.tex'
set ylabel 'Winkel [Grad]'
set xlabel 'Zeit [s]
p 'yd0_f0.txt' u ($1/1000):($2) t 'Bremse 0mm'
set output 'pd4_f0.tex'
p 'yd4_f0.txt' u ($1/1000):($2) t 'Bremse 4mm'
set output 'pd6_f0.tex'
p 'yd6_f0_neu.txt' u ($1/1000):($2) t 'Bremse 6mm'
set output 'pd8_f0.tex'
p 'yd8_f0_neu.txt' u ($1/1000):($2) t 'Bremse 8mm'
!epstopdf freqamplimax.eps
#set terminal wxt
#p 'yd2_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 2mm', 'yd4_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 4mm' w lp, 'yd6_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 6mm' w lp, 'yd8_ext.txt' u ((pi*$1)/(500*2.04659)):($2/10) t 'Bremse 8mm' w lp
#pause -1
