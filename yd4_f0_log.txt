

*******************************************************************************
Sun May 11 08:25:24 2014


FIT:    data read from 'yd4_f0.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 1883
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : nan               delta(WSSR)/WSSR   : 0
 delta(WSSR) : nan               limit for stopping : 1e-05
 lambda	  : nan

initial set of free parameter values

m               = nan
b               = 1

After 1 iterations the fit converged.
final sum of squares of residuals : nan
abs. change during last iteration : nan

degrees of freedom    (FIT_NDF)                        : 1881
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : nan
variance of residuals (reduced chisquare) = WSSR/ndf   : nan

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = nan              +/- nan          (nan%)
b               = 1                +/- nan          (nan%)


correlation matrix of the fit parameters:

               m      b      
m                 nan 
b                 nan   -nan 


*******************************************************************************
Sun May 11 08:28:52 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 


*******************************************************************************
Sun May 11 11:03:05 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 


*******************************************************************************
Sun May 11 14:28:42 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 


*******************************************************************************
Sun May 11 14:35:22 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 


*******************************************************************************
Mon May 12 17:46:38 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 


*******************************************************************************
Mon May 12 17:55:32 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 


*******************************************************************************
Mon May 12 19:16:55 2014


FIT:    data read from 'yd4_f0.txt_extrema.txt' u 1:(log($2)):3
        format = x:z:s
        #datapoints = 18
function used for fitting: f(x)
fitted parameters initialized with current variable values



 Iteration 0
 WSSR        : 848.604           delta(WSSR)/WSSR   : 0
 delta(WSSR) : 0                 limit for stopping : 1e-05
 lambda	  : 29911.3

initial set of free parameter values

m               = -1.95234e-05
b               = 4.86721

After 7 iterations the fit converged.
final sum of squares of residuals : 72.1508
rel. change during last iteration : -2.34324e-07

degrees of freedom    (FIT_NDF)                        : 16
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 2.12354
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.50943

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m               = -0.000101458     +/- 2.356e-05    (23.22%)
b               = 3.61626          +/- 0.3737       (10.33%)


correlation matrix of the fit parameters:

               m      b      
m               1.000 
b              -0.865  1.000 
