a=3.375

reset
set terminal epslatex color
#set grid
set xlabel '$t$ [s]'
set ylabel '$\phi$ [Grad]'
set key top right
set output 'd0_f0.tex'
plot "yd0_f0.txt" u ($1/1000):($2+a) title ''
set output
!epstopdf d0_f0.eps

reset
set terminal epslatex color
#set grid
set xlabel '$t$ [s]'
set ylabel '$\phi$ [Grad]'
set key top right
set output 'd4_f0.tex'
plot "yd4_f0.txt" u ($1/1000):($2+a) title ''
set output
!epstopdf d4_f0.eps

reset
set terminal epslatex color
#set grid
set xlabel '$t$ [s]'
set ylabel '$\phi$ [Grad]'
set key top right
set output 'd6_f0.tex'
plot "yd6_f0.txt" u ($1/1000):($2+a) title ''
set output
!epstopdf d6_f0.eps

reset
set terminal epslatex color
#set grid
set xlabel '$t$ [s]'
set ylabel '$\phi$ [Grad]'
set key top right
set output 'd8_f0.tex'
plot "yd8_f0.txt" u ($1/1000):($2+a) title ''
set output
!epstopdf d8_f0.eps