f(x)=m*x+b
#set fit errorvariables

reset
set terminal epslatex color
set xlabel '$t$ [s]'
set ylabel '$\ln{(\varphi)}$'
set key top right
set output 'd0_f0_extrema.tex'
set fit logfile 'd0_f0_log.log'
a=3.75
fit f(x) "yd0_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) via m,b
plot "yd0_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) w e t "Bremse 0mm logarithmisch" , f(x) t "linearer Fit"
set output
!epstopdf d0_f0_extrema.eps

set xlabel 'Zeit [s]'
set ylabel 'Winkel [Grad]'
#set key top right
set output 'd0_f0.tex'
plot "yd0_f0.txt" u ($1/1000):($2+a) title 'Bremse 0mm', exp(f(x)) t 'Einhüllende', -exp(f(x)) notitle
set output
!epstopdf d0_f0.eps


reset
set terminal epslatex color
set xlabel '$t$ [s]'
set ylabel '$\ln{(\varphi)}$'
set key bottom left
set output 'd4_f0_extrema.tex'
set fit logfile 'd4_f0_log.log'
a=4
fit f(x) "yd4_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) via m,b
plot "yd4_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) w e t "Bremse 4mm logarithmisch" , f(x) t "linearer Fit" 
set output
#!epstopdf d4_f0_extrema.eps
set terminal epslatex color
set xlabel 'Zeit [s]'
set ylabel 'Winkel [Grad]'
#set key top right
set output 'd4_f0.tex'
plot "yd4_f0.txt" u ($1/1000):($2+a) t 'Bremse 4mm'
set output
#!epstopdf d4_f0.eps

reset
set terminal epslatex color
set xlabel '$t$ [s]'
set ylabel '$\ln{(\varphi)}$'
set key bottom left
set output 'd6_f0_extrema.tex'
set fit logfile 'd6_f0_log.log'
a=3.5
fit f(x) "yd6_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) via m,b
plot "yd6_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) w e t "Bremse 6mm logarithmisch" , f(x) t "linearer Fit"
set output
#!epstopdf d6_f0_extrema.eps
set terminal epslatex color
set xlabel 'Zeit [s]'
set ylabel 'Winkel [Grad]'
#set key top right
set output 'd6_f0_log.tex'
plot "yd6_f0.txt" u ($1/1000):($2+a) title 'Bremse 6mm'
set output
#!epstopdf d6_f0.eps

reset
set terminal epslatex color
set xlabel '$t$ [s]'
set ylabel '$\ln{(\varphi)}$'
set key bottom left
set output 'd8_f0_extrema.tex'
set fit logfile 'd8_f0_log.log'
a=4
fit f(x) "yd8_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a)) via m,b
plot "yd8_f0.txt_extrema.txt" u ($1/1000):(log($2+a)):(2*$3/($2+a))  w e t "Bremse 8mm logarithmisch" , f(x) t "linearer Fit"
set output
#!epstopdf d8_f0_extrema.eps
set terminal epslatex color
set xlabel 'Zeit [s]'
set ylabel 'Winkel [Grad]'
#set key top right
set output 'd8_f0.tex'
plot "yd8_f0.txt" u ($1/1000):($2+a) title 'Bremse 8mm'
set output
#!epstopdf d8_f0.eps
