#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <cmath>

using namespace std;

int main()
{
    string dat[]={"yd0_f0.txt_extrema.txt","yd4_f0.txt_extrema.txt","yd6_f0.txt_extrema.txt","yd8_f0.txt_extrema.txt"};
    int daempfung[]={0,4,6,8};
    double  beta[]={0.0171504,0.15415,0.308362,0.465396}, betaSi[]={5.53*pow(10,-5),0.002541,0.009086,0.01356} ;
    ofstream out("periode.txt");
    double T[4],T_Si[4],logDek[4], logDekSi[4], omega_e[4], omega_e_Si[4], omega_0[4], omega_0_Si[4], omega0, omega0_Si;
    double temp1=0, temp2=0;

    for(int i=0;i<4;i++) {
        cout << i << endl;
        string a=dat[i];
        ifstream extremadat(a.c_str());
        double arr[3][200], halbperiode=0, halbperiodeSi=0;
        int k=0;
        for(int j=0;j<200;j++) {arr[0][j]=0; arr[1][j]=0; arr[2][j]=0;}
        while(!extremadat.eof()) {
            extremadat >> arr[0][k] >> arr[1][k] >> arr[2][k] ;
            k++;
        }
        halbperiode=(arr[0][k-1]-arr[0][0])/(double)k;
        for(int l=1; l<k; l++) { halbperiodeSi+=pow(halbperiode-(arr[0][l]-arr[0][l-1]),2); }
        halbperiodeSi=sqrt(halbperiodeSi/(k*(k-1)));

        T[i]=2*halbperiode/1000.;
        T_Si[i]=2*halbperiodeSi/1000.;

        omega_e[i]=2*M_PI/T[i];
        omega_e_Si[i]=2*M_PI*T_Si[i]/pow(T[i],2);

        logDek[i]=beta[i]*T[i];
        logDekSi[i]=sqrt(pow(T[i]*betaSi[i],2)+pow(T_Si[i]*beta[i],2));

        omega_0[i]=sqrt(pow(omega_e[i],2)+pow(beta[i],2));
        omega_0_Si[i]=sqrt(pow(omega_e[i]*omega_e_Si[i],2)+pow(beta[i]*betaSi[i],2))/omega_0[i];

        out << daempfung[i] << "\t" << T[i] << "\t" << T_Si[i] << "\t" << omega_e[i] << "\t" << omega_e_Si[i] << "\t";
        out << logDek[i] << "\t" << logDekSi[i] << "\t" << omega_0[i] << "\t" << omega_0_Si[i] << endl;

        cout << omega_e[i] << "\t" << omega_0[i] << "\t" << omega_0_Si[i] << endl;
        extremadat.close();
        cout << endl;
    }
    for(int i=0;i<4;i++) {
        temp1+=(omega_0[i]/pow(omega_0_Si[i],2));
        temp2+=pow(omega_0_Si[i],-2);
    }
    omega0=temp1/temp2;
    omega0_Si=pow(temp2,-0.5);

    cout << omega0 << "\t" << omega0_Si << endl;
    out.close();

    return 0;
}

